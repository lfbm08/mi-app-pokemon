function f_Regresar(){
	window.open('../index.html', "_self");
}

function f_Buscar(sNum) {
	let sName = document.getElementById("sName"+sNum).value;
	if (sName == "") {
		$('#modalAlert').modal('show');
	} else {
		$.ajax({ 
			method: "POST",
			url:'../Controllers/Pokedex.php', 
			data: { name: sName },
			complete: function (response) {
				if (response.responseText != "") {
					console.log(response);
					let datos = JSON.parse(response.responseText);
					document.getElementById("card"+sNum).style.display = 'block';
					document.getElementById("imagen"+sNum).src = datos.image;
					document.getElementById("nameText"+sNum).value = datos.name;
					document.getElementById("speciesList"+sNum).value = datos.evolve_to;
					document.getElementById("typesList"+sNum).innerHTML = '';
					datos.types.forEach(element => {
						var newElement= document.getElementById("typesList"+sNum);
						newElement.innerHTML += "<li>" + element + "</li>";
					});
				} else {
					$('#modalAlert').modal('show');
				}
			},
			error: function () { 
				console.log("Error en la peticion."); 
			} 
		});
	}
}

function f_Evolucionar(sNum) {
	let sSpecies = document.getElementById("speciesList"+sNum).value;
	let aSpecies = sSpecies.split(',');
	let actualName = document.getElementById("nameText"+sNum).value;
	let sEvolucion = '';
	for (let i = 0; i < aSpecies.length; i++) {
		if ((aSpecies[i] == actualName) && (i != aSpecies.length-1)) {
			sEvolucion = aSpecies[i+1];
		}
	}
	if (sEvolucion == '') {
		alert('Este Pokemon no tiene mas evoluciones.')
	} else {
		document.getElementById("sName"+sNum).value = sEvolucion;
		f_Buscar(sNum);
	}
}

function f_Eliminar(sNum){
	document.getElementById("card"+sNum).style.display = 'none';
	document.getElementById("imagen"+sNum).src = '';
	document.getElementById("nameText"+sNum).value = '';
	document.getElementById("speciesList"+sNum).value = '';
	document.getElementById("typesList"+sNum).innerHTML = '';
}

function f_Pelear() {
  let sName1 = document.getElementById("sName1").value;
  let sName2 = document.getElementById("sName2").value;
	if (sName1 == "" || sName1 == null) {
    alert('Por favor elija un pokemon para el JUGADOR 1');
  } else if (sName2 == "" || sName1 == null) {
    alert('Por favor elija un pokemon para el JUGADOR 2');
  } else {
    alert('Aqui pelean, pero no encontre donde se obtiene el poder de ataque en la API :C');
  }
}