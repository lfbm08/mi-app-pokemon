function f_Regresar(){
	window.open('../index.html', "_self");
}

function f_Buscar() {
	let sName = document.getElementById("sName").value;
	if (sName == "") {
		$('#modalAlert').modal('show');
	} else {
		$.ajax({ 
			method: "POST",
			url:'../Controllers/Pokedex.php', 
			data: { name: sName },
			complete: function (response) {
				if (response.responseText != "") {
					console.log(response);
					let datos = JSON.parse(response.responseText);
					document.getElementById("card").style.display = 'block';
					document.getElementById("imagen").src = datos.image;
					document.getElementById("nameText").value = datos.name;
					document.getElementById("speciesList").value = datos.evolve_to;
					document.getElementById("typesList").innerHTML = '';
					datos.types.forEach(element => {
						var newElement= document.getElementById("typesList");
						newElement.innerHTML += "<li>" + element + "</li>";
					});
				} else {
					$('#modalAlert').modal('show');
				}
			},
			error: function () { 
				console.log("Error en la peticion."); 
			} 
		});
	}
}

function f_Evolucionar() {
	let sSpecies = document.getElementById("speciesList").value;
	let aSpecies = sSpecies.split(',');
	let actualName = document.getElementById("nameText").value;
	let sEvolucion = '';
	for (let i = 0; i < aSpecies.length; i++) {
		if ((aSpecies[i] == actualName) && (i != aSpecies.length-1)) {
			sEvolucion = aSpecies[i+1];
		}
	}
	if (sEvolucion == '') {
		alert('Este Pokemon no tiene mas evoluciones.')
	} else {
		document.getElementById("sName").value = sEvolucion;
		f_Buscar();
	}
}

function f_Eliminar(){
	document.getElementById("card").style.display = 'none';
	document.getElementById("imagen").src = '';
	document.getElementById("nameText").value = '';
	document.getElementById("speciesList").value = '';
	document.getElementById("typesList").innerHTML = '';
}