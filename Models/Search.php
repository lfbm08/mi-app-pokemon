<?php
  function f_ConsultarDatos($sName) {
    $vCurl = curl_init();
    curl_setopt($vCurl, CURLOPT_URL, "https://pokeapi.co/api/v2/pokemon/$sName");
    curl_setopt($vCurl, CURLOPT_RETURNTRANSFER, 1);
    $sOutput = curl_exec($vCurl);
    curl_close($vCurl); 
    return($sOutput);
  }
  
  function f_ConsultarCadena($sId){
    $vCurl = curl_init();
    curl_setopt($vCurl, CURLOPT_URL, "https://pokeapi.co/api/v2/pokemon-species/$sId");
    curl_setopt($vCurl, CURLOPT_RETURNTRANSFER, 1);
    $sOutput = curl_exec($vCurl);
    curl_close($vCurl);
    $sUrl = json_decode($sOutput);
    $sEvoChain = $sUrl->{'evolution_chain'}->{'url'};
    curl_setopt($vCurl, CURLOPT_URL, $sEvoChain);
    curl_setopt($vCurl, CURLOPT_RETURNTRANSFER, 1);
    $sOutput = curl_exec($vCurl);
    return($sOutput);
  }
?>