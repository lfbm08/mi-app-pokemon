<?php
include("../Models/Search.php");

$sName = $_POST['name'];
$oResult = json_decode(f_ConsultarDatos($sName));
$aTypes = array();
if ($oResult == null) {
  echo null;
} else {
  foreach($oResult->{'types'} as $value) {
    array_push($aTypes, $value->{'type'}->{'name'});
  }
  
  $oEvol = json_decode(f_ConsultarCadena($oResult->{'id'}));
  $aChain = array();
  array_push($aChain, $oEvol->{'chain'}->{'species'}->{'name'});
  if (!empty($oEvol->{'chain'}->{'evolves_to'})) {
    foreach($oEvol->{'chain'}->{'evolves_to'} as $value) {
      array_push($aChain, $value->{'species'}->{'name'});
      if (!empty($value->{'evolves_to'})) {
        foreach($value->{'evolves_to'} as $value2) {
          array_push($aChain, $value2->{'species'}->{'name'});
        }
      }
    }
  }
  
  $oReturn = [
    'image' => $oResult->{'sprites'}->{'other'}->{'official-artwork'}->{'front_default'},
    'name' => $oResult->{'name'},
    'id' => $oResult->{'id'},
    'types' => $aTypes,
    'evolve_to' => $aChain
  ];
  echo json_encode($oReturn);
}
?>